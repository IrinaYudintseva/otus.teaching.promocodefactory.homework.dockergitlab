﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            if (_dataContext.Database.GetPendingMigrations().Any())
            {
                _dataContext.Database.Migrate();
            }

            // TODO: do not add initial data if database is not empty 
            if (_dataContext.Employees.Count() > 0)
                return;

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            var customers = FakeDataFactory.Customers;
            var preferences = FakeDataFactory.Preferences;

            _dataContext.AddRange(preferences);
            _dataContext.AddRange(customers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.CustomerPreferences(customers, preferences));
            _dataContext.SaveChanges();

        }
    }
}